<?php
require_once( "files.class.php" );

class Logs
{
    private const DIR =             'logs/';
    private const EXTENSION =       '.log';

    //INSTANCE FILES CLASS
    private $files =                null;

    private $last_file =            null;
    private $timer_start =          null;
    private $timer_end =            null;

    function __construct()
    {
        $this->files = new Files();
        if( !$this->files->dir_exists( self::DIR ) ) {
            $this->files->mkdir( self::DIR );
        }
        $this->gzip();
        $this->clean();
    }



    // ADD LOGS ------------------------------------------------------------------------------------- //
    public function add( array $line, string $filename ) : void
    {
        $this->last_file = $filename;
        $path = $this->getFile( $filename );
        foreach ( $line as $key => $value ) {
            if ( is_array( $value ) ) {
                $line[$key] = json_encode( $value );
            }
        }
        file_put_contents( $this->files->root().$path, "[".date('c')."]".implode( $line, " - " ).PHP_EOL , FILE_APPEND | LOCK_EX );
    }

    public function add_to_last_file( array $line ) : void
    {
        $path = $this->getFile( $this->last_file );
        file_put_contents( $this->files->root().$path, "[".date('c')."]".implode( $line, " - " ).PHP_EOL , FILE_APPEND | LOCK_EX );
    }



    // TIMER ------------------------------------------------------------------------------------- //
    public function start_timer() : void
    {
        $this->timer_start = time();
    }

    public function end_timer() : void
    {
        $this->timer_end = time();
    }

    public function timer()
    {
        if ( $this->timer_start != null && $this->timer_end != null ) {
            if ( $this->timer_start > $this->timer_end ) {
                return "Timer start bad init.";
            } else {
                $timer = $this->timer_end - $this->timer_start;
                return $timer;
            }
        } else {
            return "Timer not correctly init.";
        }
    }



    // FUNCTIONS ------------------------------------------------------------------------------------- //
    private function getFile( string $filename )
    {
        $date = date('YW');
        $path = self::DIR.$date."-".$filename.self::EXTENSION;
        if ( !$this->files->file_exists( $path ) ) {
            $this->setFile( $path );
        }
        return $path;
    }

    private function setFile( string $path ) : void
    {
        $this->files->mkfile( $path );
    }

    private function gzip()
    {
        foreach( scandir( $this->files->root().self::DIR ) as $file ) {
            if ( $file !== "." &&  $file !== ".." ) {
                if ( preg_match( "#^(?!".date( 'YW', strtotime( 'now' ) ).")^.*\.log$#", $file ) ) {
                    $this->files->gzip_file( self::DIR.$file );
                }
            }
        }
    }

    private function clean()
    {
        foreach( scandir( $this->files->root().self::DIR ) as $file ) {
            if ( $file !== "." &&  $file !== ".." ) {
                if ( preg_match( "#.gz$#", $file ) ) {
                    $mtime = $this->files->filemtime( self::DIR.$file );
                    if ( $mtime < strtotime( '-6 week' ) ) {
                        $this->files->delete_file( self::DIR.$file );
                    }
                }
            }
        }
    }
}