<?php

class Files {
    private $DIR_MOD =       775;
    private $FILE_MOD =      664;



    // CREATE ------------------------------------------------------------------------------------- //
    public function mkdir( string $path ) : void
    {
        $global_path = $this->root().$path;
        mkdir( $global_path, $this->DIR_MOD, true );
    }

    public function mkfile( string $path ) : void
    {
        $global_path = $this->root().$path;
        file_put_contents( $global_path, null );
    }



    // EXISTS ------------------------------------------------------------------------------------- //
    public function dirExists( string $dirname )
    {
        if ( strstr( $path, $this->root() ) ) {
            $global_path = $dirname;
        } else {
            $global_path = $this->root().$dirname;
        }
        $realpath = $this->getRealPath( $global_path );
        if ( file_exists( $realpath ) && is_dir( $realpath ) ) {
            return true;
        }
        return false;
    }

    public function fileExists( string $path )
    {
        if ( strstr ( $path, $this->root() ) ) {
            $global_path = $path;
        } else {
            $global_path = $this->root().$path;
        }
        $realpath = $this->getRealPath( $global_path );
        if ( file_exists( $realpath ) && is_file( $realpath ) ) {
            return true;
        }
        return false;
    }



    // DELETE ------------------------------------------------------------------------------------- //
    public function deleteFile( string $path ) : void
    {
        if ( $this->fileExists( $path ) ) {
            unlink( $path );
        }
    }



    // FUNCTIONS ------------------------------------------------------------------------------------- //
    public function root()
    {
        return __DIR__.'/../';
    }

    public function getRealPath( string $path )
    {
        $path = preg_replace( "#\/{2,}#", "/", $path );
        return realpath( $path );
    }

    public function filemtime( string $path )
    {
        if ( strstr ( $path, $this->root() ) ) {
            $global_path = $path;
        } else {
            $global_path = $this->root().$path;
        }
        return filemtime( $global_path );
    }

    public function compress( string $filename ) : void
    {
        if ( $this->fileExists( $filename ) ) {
            $file = $this->root().$filename;
            $gzfile = $file.".gz";
            $gzfp = gzopen( $gzfile, 'w9' );
            if ( class_exists( 'SplFileObject' ) ) {
                $iterator = new SplFileObject( $file );
                foreach ( $iterator as $line ) {
                    gzwrite( $gzfp, $line );
                }
                unset( $iterator );
            } else {
                $gzfp = gzopen( $gzfile, 'w9' );
                $fp = fopen( $file, 'r' );
                stream_copy_to_stream( $fp, $gzfp );
                fclose( $fp );
            }
            $pointer  = gztell( $gzfp );
            gzclose( $gzfp );
            if ( $this->fileExists( $gzfile ) && $pointer != 0 ) {
                unlink( $this->root().$filename );
            } else {
                unlink( $gzfile );
            }
        }
    }
}